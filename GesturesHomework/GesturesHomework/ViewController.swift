//
//  ViewController.swift
//  GesturesHomework
//
//  Created by Daniel Freire on 1/21/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func leftSwipe(_ sender: UIScreenEdgePanGestureRecognizer) {
        customView.backgroundColor = .blue
    }
    
    @IBAction func rightSwipe(_ sender: UIScreenEdgePanGestureRecognizer) {
        customView.backgroundColor = .red
    }
}

