//
//  PinchGestureViewController.swift
//  GesturesHomework
//
//  Created by Daniel Freire on 1/21/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class PinchGestureViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var scaleLabel: UILabel!
    @IBOutlet weak var velocityLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        
        let scale = sender.scale
        let velocity = sender.velocity
        let state = sender.state
        
        if state == .began || state == .changed {
            scaleLabel.text = "\(scale)"
            velocityLabel.text = "\(velocity)"
            
            customView.transform = customView.transform.scaledBy(x: scale, y: scale)
            sender.scale = 1
            
            if (velocity >= 10) {
                customView.frame = self.view.frame
            }
        }
    }
}
